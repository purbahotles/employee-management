import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface Employee {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: string;
  status: string;
  group: string;
  description: Date;
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})
export class EmployeeListComponent implements OnInit, AfterViewInit {
  employees: Employee[] = [];
  dataSource = new MatTableDataSource<Employee>();
  displayedColumns: string[] = [
    'username',
    'firstName',
    'lastName',
    'email',
    'birthDate',
    'basicSalary',
    'status',
    'group',
    'description',
  ];
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit(): void {
    this.generateDummyData();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  randomDate(start: Date, end: Date): Date {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }

  randomString(length: number): string {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    let result = '';
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  }

  randomDouble(min: number, max: number): number {
    return Math.random() * (max - min) + min;
  }

  transform(value: number): string {
    if (!isNaN(value)) {
      const formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR', // Rupiah
        minimumFractionDigits: 0,
      });
      return formatter.format(value);
    } else {
      return '';
    }
  }

  generateDummyData(): void {
    for (let i = 0; i < 100; i++) {
      const basicSalary = this.randomDouble(30000, 90000);
      const formattedBasicSalary = this.transform(basicSalary) || ''; // Use your custom pipe
      const employee: Employee = {
        username: this.randomString(8),
        firstName: this.randomString(8),
        lastName: this.randomString(8),
        email: `${this.randomString(5)}@example.com`,
        birthDate: this.randomDate(new Date(1980, 0, 1), new Date(2000, 11, 31)),
        basicSalary: formattedBasicSalary,
        status: Math.random() < 0.5 ? 'Active' : 'Inactive',
        group: this.randomString(5),
        description: new Date(),
      };
      this.employees.push(employee);
    }
    this.dataSource.data = this.employees; // Assign the data to the dataSource
  }
}
